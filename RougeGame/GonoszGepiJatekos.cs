﻿using RougeGame.Jatek.Jatekter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RogueGame.Jatek.Szabalyok
{
    class GonoszGepiJatekos : GepiJatekos
    {
        public GonoszGepiJatekos(string nev, int x, int y, JatekTer ter) : base(nev, x, y, ter) { }
        public override char Alak =>'\u2642';
        public override void utkozes(JatekElem elem) {
            base.utkozes(elem);
            if (this.Aktiv && (elem is Jatekos)) {
                (elem as Jatekos).serul(10);
            }
        }
    }
}
