﻿using System.Collections.Generic;
using System;
using RogueGame.Jatek.Megjelenites;

namespace RougeGame.Jatek.Jatekter
{
    class JatekTer : IMegjelenitheto
    {
        const int MAX_ELEMSZAM = 1000;

        int elemN;
        List<JatekElem> elemek;
        int meretX;
        int meretY;

        public int MeretY { get; }
        public int MeretX { get; }

        public int[] MegjelenitendoMeret {
            get  {
                int[] vmi = new int[] { meretX, meretY };
                return vmi;
            }
        }

        public JatekTer(int x, int y) {
            this.meretX = x;
            this.meretY = y;
            elemek = new List<JatekElem>(MAX_ELEMSZAM);
            elemN = 0;
        }

        public void felvetel(JatekElem elem) {
            ++elemN;
            this.elemek.Add(elem);
        }

        public void torles(JatekElem elem) {
            --elemN;
            this.elemek.Remove(elem);
        }

        public List<JatekElem> MegadottHelyenLevok (int x, int y, int tavolsag){
            List<JatekElem> returnList = new List<JatekElem>();
            foreach (JatekElem item in this.elemek)
            {
                if ( Math.Sqrt(Math.Pow((item.X - x), 2) + Math.Pow((item.Y - y), 2)) < tavolsag) {
                    returnList.Add(item);
                }
            }

            return returnList;
        }
        public List<JatekElem> MegadottHelyenLevok(int x, int y) {
            return this.MegadottHelyenLevok(x, y, 0);
        }

        public IKirajzolhato[] MegjelenítendőElemek()
        {
            int sum = 0;
            List<IKirajzolhato> vissza = new List<IKirajzolhato>();
            foreach (JatekElem item in elemek)
            {
                if (item is IKirajzolhato) {
                    ++sum;
                    vissza.Add(item as IKirajzolhato);
                }
            }
            IKirajzolhato [] viss = new IKirajzolhato[sum];
            for (int i = 0; i < sum; ++i) {
                viss[i] = vissza[i]; 
            }
            
            return viss;
        }
    }
}
