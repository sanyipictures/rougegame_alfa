﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RogueGame.Jatek.Automatizmus;
using RougeGame.Jatek.Jatekter;

namespace RogueGame.Jatek.Szabalyok
{
    class GepiJatekos : Jatekos, IAutomatikusanMukodo
    {
        public static Random rnd = new Random();
        public GepiJatekos(string nev, int x, int y, JatekTer ter) : base(nev, x, y, ter){}
        public virtual void Mozgas() { 
            this.megy(rnd.Next(4), rnd.Next(4));
        }
        public int MukodesIntervallum { get => 2; }
        public void Mukodik()
        {
            try {
                this.Mozgas();
            }
            catch (MozgasHelyHianyMiattNemSikerultKivetel ex) {
                this.Mozgas();
            }
        }

        public override char Alak => '\u2640';

        
    }
}
