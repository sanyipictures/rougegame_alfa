﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RogueGame.Jatek.Megjelenites

{
    interface IMegjelenitheto
    {
        int[] MegjelenitendoMeret { get; }
        IKirajzolhato[] MegjelenítendőElemek();

    }
}
