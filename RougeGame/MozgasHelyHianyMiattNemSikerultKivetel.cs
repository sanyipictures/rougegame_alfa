﻿using RougeGame.Jatek.Jatekter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RogueGame.Jatek.Szabalyok
{
    class MozgasHelyHianyMiattNemSikerultKivetel : MozgasNemSikerultKivetel
    {
        private List<JatekElem> elemek;
        public List<JatekElem> Elemek { get; }

        public MozgasHelyHianyMiattNemSikerultKivetel(JatekElem jelem, int x, int y) : base(jelem, x, y) {
            this.elemek = new List<JatekElem>();
        }
    }
}
