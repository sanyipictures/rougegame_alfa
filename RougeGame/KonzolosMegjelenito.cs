﻿using RogueGame.Jatek.Automatizmus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RogueGame.Jatek.Megjelenites
{
    class KonzolosMegjelenito : IAutomatikusanMukodo
    {
        private IMegjelenitheto forras;
        private int posX;
        private int posY;

        public KonzolosMegjelenito(int px, int py, IMegjelenitheto forras) {
            this.posX = px;
            this.posY = py;
            this.forras = forras;
        }

        public void Mukodik()
        {
            this.megjelenites();
        }

        int IAutomatikusanMukodo.MukodesIntervallum => 1;

        public void megjelenites() {
            IKirajzolhato[] kirajz = forras.MegjelenítendőElemek();
            int[] meret = forras.MegjelenitendoMeret;
            for (int i = 0; i < meret[0]; i++)
            {
                for (int j = 0; j < meret[1]; j++)
                {
                    for (int k = 0; k < kirajz.Length; k++)
                    {
                        if (kirajz[k].X == i && kirajz[k].Y == j)
                        {
                            SzalbiztosKonzol.KiirasXY(kirajz[k].X, kirajz[k].X, ' ');
                        }
                        else {
                            String horizont = "";
                            
                            for (int l = 0; l < posX; ++l) {
                                horizont += " ";
                            }
                            for (int l = 0; l < posY; ++l) {
                                Console.WriteLine();
                            }
                            Console.WriteLine(horizont + kirajz[k].Alak);
                        }
                    }
                }
            }
            
        }
    }
}
