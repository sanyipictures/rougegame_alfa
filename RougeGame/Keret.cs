﻿using RogueGame.Jatek.Automatizmus;
using RogueGame.Jatek.Megjelenites;
using RogueGame.Jatek.Szabalyok;
using RougeGame;
using RougeGame.Jatek.Jatekter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RogueGame.Jatek.Keret
{
    class Keret
    {
        private const int PALYA_MERET_X = 21;
        private const int PALYA_MERET_Y = 11;
        private const int KINCSEK_SZAMA = 10;
        private OrajelGenerator orajel;
        private bool jatekVege;
        private JatekTer ter;
        private int megtalaltKincsek;

        public Keret() {
            this.orajel = new OrajelGenerator();
            this.ter = new JatekTer(PALYA_MERET_X, PALYA_MERET_Y);
            this.PalyaGeneralas();
            this.jatekVege = false;
            this.megtalaltKincsek = 0;
        }
        private void JatekosValtozasTortent(Jatekos player, int pontszam, int eletero) {
            if (eletero == 0) {
                this.jatekVege = true;
            }
        }
        private void KincsFelvetelTortent(Kincs kincs, Jatekos player) {
            ++megtalaltKincsek;
            if (megtalaltKincsek == KINCSEK_SZAMA) {
                jatekVege = true;
            }
        }

        private void PalyaGeneralas() {
            for (int i = 0; i < 21; ++i) {
                this.ter.felvetel(new Fal(i,0,ter));
                this.ter.felvetel(new Fal(i, 10, ter));
            }
            for (int i = 0; i < 11; ++i)
            {
                this.ter.felvetel(new Fal(0, i, ter));
                this.ter.felvetel(new Fal(20, i, ter));
            }
            
            Random rnd = new Random();
            for (int i = 0; i < KINCSEK_SZAMA; ++i) {
                Kincs k = new Kincs(rnd.Next(1, 20),rnd.Next(1, 10), ter);
                k.ev += this.KincsFelvetelTortent;
                this.ter.felvetel(k); 
            }
        }
        public void futtatas() {
            KonzolosEredmenyAblak keaobj = new KonzolosEredmenyAblak(0, 12, 5);
            
            KonzolosMegjelenito megjel = new KonzolosMegjelenito(0, 0, ter);
            Jatekos Bela = new Jatekos("BÉLUKÁM!!!",1, 1, this.ter);
            keaobj.JatekosFeliratkozas(Bela);
            Bela.JatekosValtozas += this.JatekosValtozasTortent;
            IAutomatikusanMukodo Kati = new GepiJatekos("Kati", 5, 5, this.ter);
            IAutomatikusanMukodo Laci = new GonoszGepiJatekos("Laci", 7, 7, this.ter);
            KonzolosMegjelenito megjel2 = new KonzolosMegjelenito(25, 0, Bela);
            this.orajel.Felvetel(Kati);
            this.orajel.Felvetel(Laci);
            this.orajel.Felvetel(megjel);
            this.orajel.Felvetel(megjel2);
            do
            {
                //megjel.megjelenites();
                //megjel2.megjelenites();
                ConsoleKeyInfo key = Console.ReadKey(true);
                try
                {
                    if (key.Key == ConsoleKey.LeftArrow) Bela.megy(-1, 0);
                    if (key.Key == ConsoleKey.RightArrow) Bela.megy(1, 0);
                    if (key.Key == ConsoleKey.UpArrow) Bela.megy(0, -1);
                    if (key.Key == ConsoleKey.DownArrow) Bela.megy(0, 1);
                    if (key.Key == ConsoleKey.Escape) jatekVege = true;
                }
                catch (MozgasHelyHianyMiattNemSikerultKivetel ex) {
                    System.Console.Beep(500 + ex.Elemek.Count * 100, 10);
                }
            } while (!jatekVege);
        }
    }
}
