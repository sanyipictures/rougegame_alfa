﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RogueGame.Jatek.Megjelenites
{
    interface IKirajzolhato
    {
        int X { get; }
        int Y { get; }
        char Alak { get; }
    }
}
