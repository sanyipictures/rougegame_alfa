﻿using RogueGame.Jatek.Szabalyok;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RogueGame.Jatek.Megjelenites
{
    class KonzolosEredmenyAblak
    {
        private int pozX;
        private int pozY;
        private int maxSorSzam;
        private int sor;

        public KonzolosEredmenyAblak(int pozx, int pozy, int maxsor) {
            pozX = pozx;
            pozY = pozy;
            maxSorSzam = maxsor;
            sor = 0;
        }
        public void JatekosValtozasTortent(Jatekos bela, int ujpont, int ujhp) {
            SzalbiztosKonzol.KiirasXY(pozX,pozY,bela.Alak);
            sor = maxSorSzam % ++sor; ;
        }
        public void JatekosFeliratkozas(Jatekos matyi) {
            matyi.JatekosValtozas += this.JatekosValtozasTortent;
        }
    }
}
