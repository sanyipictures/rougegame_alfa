﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RougeGame.Jatek.Jatekter
{
    abstract class JatekElem
    {
        int x;
        int y;
        protected JatekTer ter;
        public int X { get; set; }
        public int Y { get; set; }
        abstract public double Meret { get; set; }

        public JatekElem(int x, int y, JatekTer asd) {
            this.x = x;
            this.y = y;
            this.ter = asd;
            ter.felvetel(this);
        }

        abstract public void utkozes(JatekElem elem);
    }
}
