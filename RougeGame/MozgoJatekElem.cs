﻿using RogueGame.Jatek.Szabalyok;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RougeGame.Jatek.Jatekter
{
    abstract class MozgoJatekElem : JatekElem
    {
        private bool aktiv;

        public bool Aktiv { get; set; }

        public MozgoJatekElem(int x, int y, JatekTer ter) : base(x,y,ter) {}

        public void AtHelyet(int ujx, int ujy) {
            List<JatekElem> exist = this.ter.MegadottHelyenLevok(ujx, ujy);
            foreach (JatekElem item in exist)
            {
                item.utkozes(this);
                if (!this.aktiv) {
                    throw new MozgasHalalMiattNemSikerultKivetel(item,item.X, item.Y);
                }
                this.utkozes(item);
                if (!this.aktiv)
                {
                    throw new MozgasHalalMiattNemSikerultKivetel(item, item.X, item.Y);
                }
            }
            if(this.aktiv)
            exist  = this.ter.MegadottHelyenLevok(ujx, ujy);

            double sum = 0;
            foreach (JatekElem item in exist) {
                sum += item.Meret;
            }

            if ((sum = sum + ujx + ujx) < 1)
            {
                this.AtHelyet(ujx, ujy);
            }
            else {
                throw new MozgasHelyHianyMiattNemSikerultKivetel(this,ujx,ujy);
            }
        }
    }
}
