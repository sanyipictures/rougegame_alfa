﻿using RogueGame.Jatek.Megjelenites;
using RougeGame.Jatek.Jatekter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RogueGame.Jatek.Szabalyok
{
    class Fal : RogzitettJatekElem, IKirajzolhato
    {
        public Fal(int x, int y, JatekTer ter) : base(x,y,ter) { }

        public override double Meret { get => 1; set => this.Meret = 1; }

        public char Alak => '\u2593';

        public override void utkozes(JatekElem elem){}
    }
}
