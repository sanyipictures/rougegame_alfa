﻿using RougeGame.Jatek.Jatekter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RogueGame.Jatek.Szabalyok
{
    class MozgasNemSikerultKivetel : Exception
    {
        private JatekElem jatekElem;
        public JatekElem JatekElem { get; }
        private int x;
        public int X { get; }
        private int y;
        private JatekElem jelem;
        public int Y { get; }

        public MozgasNemSikerultKivetel(JatekElem jelem, int x, int y){
            this.jatekElem = jelem;
            this.x = x;
            this.y = y;
        }

    }
}
