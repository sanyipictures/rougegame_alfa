﻿using RogueGame.Jatek.Megjelenites;
using RogueGame.Jatek.Szabalyok;
using RougeGame.Jatek.Jatekter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RougeGame
{
    class Kincs : RogzitettJatekElem, IKirajzolhato
    {
        public override double Meret { get => 1; set => base.Meret = value; }

        public char Alak => '\u2666';
        public delegate void JatekosValtozasKezelo(Jatekos player, int pontszam, int eletero);
        public event KicsFelvetelKezelo ev;

        public Kincs(int x, int y, JatekTer ter) : base(x, y, ter) { }

        public override void utkozes(JatekElem elem)
        {
            if (elem is Jatekos) {
                (elem as Jatekos).pontotSzerez(50);
                ev?.Invoke(this, elem as Jatekos);
                //if (ev != null) {
                //    Console.WriteLine("Kincs felvéve!");
                //}
            }
            this.ter.torles(this);
        }
        
    }
}
