﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RogueGame.Jatek.Automatizmus
{
    interface IAutomatikusanMukodo
    {
        void Mukodik();
        int MukodesIntervallum { get; }
    }
}
