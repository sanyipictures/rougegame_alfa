﻿using RogueGame.Jatek.Megjelenites;
using RougeGame;
using RougeGame.Jatek.Jatekter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static RougeGame.Kincs;

namespace RogueGame.Jatek.Szabalyok
{
    delegate void KicsFelvetelKezelo(Kincs amit, Jatekos aki);
    class Jatekos : MozgoJatekElem, IKirajzolhato, IMegjelenitheto
    {
        private String nev;
        private int eletero;
        private int pontszam;

        public String Nev { get; }
        public override double Meret { get => 0.2; set => this.Meret = value; }

        public virtual char Alak => '\u263A';

        public int[] MegjelenitendoMeret => this.ter.MegjelenitendoMeret;

        public JatekosValtozasKezelo JatekosValtozas;

        public Jatekos(String nev, int x, int y, JatekTer ter) : base(x, y, ter) {
            this.nev = nev;
            this.eletero = 100;
            this.pontszam = 0;
        }

        public override void utkozes(JatekElem elem) { }

        public void serul(int amount) {
            if (this.eletero > 0) {
                this.eletero = eletero - amount;
                if (this.eletero < 0)
                {
                    this.eletero = 0;
                    this.Aktiv = false;
                }
                if (this.eletero == 0)
                    this.Aktiv = false;
                JatekosValtozas?.Invoke(this,this.pontszam, this.eletero);
            }
        }
        public void pontotSzerez(int amount) {
            this.pontszam += amount;
            JatekosValtozas?.Invoke(this, this.pontszam, this.eletero);
        }
        public void megy(int x, int y) {
            if ((x == 0 || x == -1 || x == 1) && (y == -1 || y == 0 || y == 1)) {
                this.AtHelyet(x, y);
            }
        }

        public IKirajzolhato[] MegjelenítendőElemek()
        {
            int sum = 0;
            List<JatekElem> asd = this.ter.MegadottHelyenLevok(X,Y,5);
            List<JatekElem> asd2 = new List<JatekElem>();
            foreach (JatekElem item in asd)
            {
                if (item is IKirajzolhato) {
                    ++sum;
                    asd2.Add(item);
                }
            }
            IKirajzolhato[] vissza = new IKirajzolhato[sum];
            for (int i = 0; i < sum; ++i)
            {
                vissza[i] = (asd2[i] as IKirajzolhato);
            }
            return vissza;
        }
    }
}
